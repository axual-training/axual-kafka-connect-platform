#!/bin/sh
#
# Use this script to delete the JDBC Connector to read account entries
#
docker-compose exec connect \
curl -X DELETE http://connect:8085/connectors/jdbc-accountentries

