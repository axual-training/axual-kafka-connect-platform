#!/bin/sh
#
# Use this script to create the JDBC Connector to read account authorizations`
#
docker-compose exec connect \
curl -X DELETE http://connect:8085/connectors/jdbc-authorizations
